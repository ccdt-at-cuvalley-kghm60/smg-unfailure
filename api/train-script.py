# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
import pandas as pd
import numpy as np

NUM_WORKERS = 0


# %%
df = pd.DataFrame()
NUM_SAMPLES = 10_000
df['ENGRPM'] = [np.random.randint(1500, 2200) for i in range(NUM_SAMPLES)]
df['ENGOLIP'] = [np.random.randint(0,10) for i in range(NUM_SAMPLES)]
df['GROLIP'] = [np.random.randint(50,70) for i in range(NUM_SAMPLES)]
df['GROLIT'] = [np.random.randint(80,95) for i in range(NUM_SAMPLES)]
df['SPEED'] = [np.random.randint(0,15) for i in range(NUM_SAMPLES)]
df['ENGTPS'] = [np.random.randint(0,10) for i in range(NUM_SAMPLES)]
df['target'] = [0] * NUM_SAMPLES


# %%
df_failures = pd.DataFrame()
NUM_SAMPLES = 10_000
df_failures['ENGRPM'] = [np.random.randint(1500, 2200) for i in range(NUM_SAMPLES)]
df_failures['ENGOLIP'] = [np.random.randint(0,10) for i in range(NUM_SAMPLES)]
df_failures['GROLIP'] = [np.random.randint(65,80) for i in range(NUM_SAMPLES)]
df_failures['GROLIT'] = [np.random.randint(80,95) for i in range(NUM_SAMPLES)]
df_failures['SPEED'] = [np.random.randint(0,15) for i in range(NUM_SAMPLES)]
df_failures['ENGTPS'] = [np.random.randint(0,10) for i in range(NUM_SAMPLES)]
df_failures['target'] = [1] * NUM_SAMPLES


# %%
df.head()


# %%



# %%



# %%
df_failures.head()


# %%
from sklearn import preprocessing


# %%
training_data = pd.concat((df, df_failures))


# %%
training_data['ENGRPM'] = training_data['ENGRPM'] /training_data['ENGRPM'].abs().max()
training_data['ENGOLIP'] = training_data['ENGOLIP'] /training_data['ENGOLIP'].abs().max()
GROILP_MAX = training_data['GROLIP'].abs().max()
training_data['GROLIP'] = training_data['GROLIP'] / GROILP_MAX
training_data['GROLIT'] = training_data['GROLIT'] /training_data['GROLIT'].abs().max()
training_data['SPEED'] = training_data['SPEED'] /training_data['SPEED'].abs().max()
training_data['ENGTPS'] = training_data['ENGTPS'] /training_data['ENGTPS'].abs().max()


# %%
training_data.head()


# %%
training_data.head()


# %%
training_data = training_data.sample(frac=1).reset_index(drop=True)


# %%
validation_data = training_data[:1000]
training_data = training_data[1000:]


# %%
validation_data.shape


# %%
training_data.shape


# %%
training_data.head()


# %%
import torch
from torch.utils.data import Dataset
from torch.utils.data import DataLoader


# %%
class SGM_Dataset(Dataset):
    def __init__(self, data):
        self.data = data
    
    def __len__(self, ):
        return self.data.shape[0]
    
    def __getitem__(self, idx):
        input_ =  np.array(self.data.iloc[idx].tolist()[:-1])
        target =  self.data.iloc[idx].tolist()[-1]
        
#         if target == 0:
#             output = np.array([1,0])
#         else:
#             output = np.array([0,1])
        return {
            'input' : input_, 
            'target': np.array(target)
        }


# %%
train_dataset = SGM_Dataset(training_data)
train_loader = DataLoader(train_dataset, batch_size=128, num_workers=NUM_WORKERS, shuffle=True)


# %%
import torch
import torch.nn as nn
import torch.nn.functional as F



class Model(nn.Module):
    def __init__(self, hidden_size):
        super().__init__()
        self.l1 = nn.Linear(6, hidden_size)
        self.l2 = nn.Linear(hidden_size, hidden_size)

        self.l_output = nn.Linear(hidden_size, 2)

    def forward(self, x):
        x = F.leaky_relu(self.l1(x))
        x = F.leaky_relu(self.l2(x))  
        return self.l_output(x)


# %%
model = Model(100)


# %%
import torch.optim as optim
criterion = nn.CrossEntropyLoss()

optimizer = optim.Adam(model.parameters(), lr=0.00001)


# %%
for epoch_idx in range(3):
    for batch in train_loader:
        # zero the parameter gradients
        optimizer.zero_grad()
        inputs = batch['input'].float()
        outputs = model(inputs)
        loss = criterion(outputs, batch['target'].long())
        loss.backward()
        optimizer.step()
        # print(loss.item())


# %%
valid_dataset = SGM_Dataset(validation_data)
valid_loader = DataLoader(valid_dataset, batch_size=128, num_workers=NUM_WORKERS, shuffle=True)


# %%
model.eval()
with torch.no_grad():
    for batch in valid_loader:
            inputs = batch['input'].float()
            outputs = model(inputs)
            loss = criterion(outputs, batch['target'].long())
            print(loss.item())


# %%
inputs.shape


# %%
training_data.iloc[1].tolist()


# %%
ID = 150
print(training_data.iloc[ID])
input_ = torch.from_numpy(np.array(training_data.iloc[ID].tolist()[:-1]))
input_


# %%
# GROILP_IDX = 2
# input_[GROILP_IDX] = 80 / GROILP_MAX


# %%
input_


# %%
# input_[None].shape


# %%
GROILP_IDX = 2
input_[GROILP_IDX] = 80 / GROILP_MAX

res = torch.softmax(model(input_[None].float()), dim=1)
chance_of_failure_soon = res.detach().numpy()[0][1]
print('OK' if chance_of_failure_soon < .5 else  'WARN')
res


# %%
batch = next(iter(train_loader))
model.eval()
model.cpu()
traced_script_module = torch.jit.trace(model, (batch['input'].float().cpu()))
traced_script_module.save(f'model')


# %%
model = torch.jit.load('model')


# %%



